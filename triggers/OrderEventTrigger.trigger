trigger OrderEventTrigger on Order_Event__e (after insert) { 
    // List to hold all tasks to be created.
    List<Task> tasks = new List<Task>();

    // Get user Id for task owner
   // User usr = [SELECT Id FROM User WHERE Name='Admin User' LIMIT 1];


    // Iterate through each notification.
    for (Order_Event__e event : Trigger.New) {
        if (event.Has_Shipped__c == true) {
            // Create task 
            Task tk = new Task(Priority='Medium', status='New',subject='Follow up on shipped order ' + event.Order_Number__c, ownerId=UserInfo.getUserId());
            tasks.add(tk);

        }
   }

    // Insert all tasks corresponding to events received.
    insert tasks;
}