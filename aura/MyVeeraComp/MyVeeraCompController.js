({
	getInput : function(component, event, helper) {       
        
          var validForm = component.find('FormVal').reduce(function(validSoFar, inputCmp){
	            // Displays error messages for invalid fields
	            inputCmp.showHelpMessageIfInvalid();
	            return validSoFar && inputCmp.get('v.validity').valid;
	        }, true);
	        // If we pass error checking, do some real work
	        if(validForm){
                
                 // Get the Username from Component
                var user = component.get("v.Username");        
                var Pass = component.get("v.Password");    
                var myURl= 'picklist_check';
                console.log('>>>>>user>>>>'+user);
                console.log('>>>>>>>>pass>>>>>>>>>>>>'+Pass);
               //Calling controller 
               // Create the action
                    var action = component.get("c.checkPortal");
                   
                action.setParams({ username : user,password:Pass,startUrl:myURl });

                    // Add callback behavior for when response is received
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        
                        if (state === "SUCCESS") {
                            //Redirect to portal home page
                            console.log('>>>USer validated in controller<<<'+response.getReturnValue());
                           

                        }
                        else {
                            console.log("Failed with state: " + state);
                        }
                    });
                
                    // Send action off to be executed
                    $A.enqueueAction(action);
                
                
            }
	}
})