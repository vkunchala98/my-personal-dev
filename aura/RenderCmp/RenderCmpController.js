({
    ToggleCollapseHandler : function(component, event) { 
        var index = event.target.data.collapseIndex;
        var existingText = component.get("v.collapseText["+index+"]");
       var container = component.find("containerCollapsable")[index];
        if(existingText === "[ - ]"){
             component.set("v.collapseText["+index+"]","[ + ]");
            $A.util.addClass(container, 'hide');
        }else{
            component.set("v.collapseText["+index+"]","[ - ]"); 
            $A.util.removeClass(container, 'hide'); 
        }  
    }
})