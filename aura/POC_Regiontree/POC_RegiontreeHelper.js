({
	 displayOptionsLocation: function (component, searchKey , Language) {
        var action = component.get("c.getAddressAutoComplete");
        action.setParams({
            "input": searchKey,
            "langug": Language,
            "types": '(regions)'
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var options = JSON.parse(response.getReturnValue());
                var predictions = options.predictions;
                var addresses = [];
                if (predictions.length > 0) {
                    for (var i = 0; i < predictions.length; i++) {
                       // var b;
                        // for(var j=0;j<predictions[i].terms.length;j++){
                         //   b.push(predictions[i].terms[i].toString());
                           //  console.log('^^^^^^terms Array^^^'+predictions[i].terms[i]);
                            
                       // }
                        
                     console.log('^^^^^^terms Array^^^'+predictions[i].terms[0]);
                      //  if(predictions[i].terms.length>0){
                        var bc =[];
                        for(var j=0;j<predictions[i].terms.length;j++){
                            
                          //  bc.add(predictions[i].terms[i].Offset);
                            bc.push(predictions[i].terms[j].offset , predictions[i].terms[j].value );
                            console.log('@@@offset inside@@@@'+predictions[i].terms[j].offset);
                            console.log('@@@value inside@@@@'+predictions[i].terms[j].value);
                        
                       // }
                       console.log('@@@@bc value@@'+bc);
                            }
                        addresses.push(
                            {
                                value: predictions[i].types[0],
                                PlaceId: predictions[i].place_id,
                                locaval: bc,
                                label: predictions[i].description                              
                            });
                       
                    }
                    console.log('@@#@#@##@#'+addresses);
                    component.set("v.filteredOptions", addresses);
                }
            }
        });
        $A.enqueueAction(action);
    },
    sendSelectedOption:function(component,locaval2){
        
        
       console.log('@@@@@Selected Place holder Terms@@@@@@@@'+locaval2);
        
        
          var action1 = component.get("c.processWebResp");
        action1.setParams({
            "Res": locaval2
        });

        action1.setCallback(this, function (response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('This is webservice resp >>');
                
            }
            
            
        });
        
        $A.enqueueAction(action1);
       // console.log('&&&&&&&&&new method&&&****'+localval2[0]);
     //   console.log(Object.keys(localval)); // console: ['0', '1', '2']
        /* for(var key in locaval) {
              var value = objects[key];
                console.log('<><><>inside <>>??????'+value);
             } */
        
    },
    displayOptionDetails: function(component,placeid){
        var self = this;
        console.log('@@#@#@#@#@#@#@#@#'+placeid);
         var action1 = component.get("c.getAddressDetails");
        action1.setParams({
            "PlaceId": placeid
        });

        action1.setCallback(this, function (response) {
            
             var state = response.getState();
            if (state === "SUCCESS") {
                var options = JSON.parse(response.getReturnValue());
                 var Addressdet = options.result;
              
                console.log('?????Address Components??????');
                var key = "address_components";
                var o = Addressdet[key]  // value2
                for(var prop in o) {
                  console.log(prop,o[prop]);  
                }
                 console.log('?????Formated Address??????'+o);
                self.insertRecords(component,o);
                
                //  var key1="formatted_address";
                // var o1=Addressdet[key1]  // value2              
                //  console.log('&&'+Object.values('formatted_address'));  
                
                
                //var match = "formatted_address"
                //var val = Addressdet.find( function(item) { return item.key == match } );
                //console.log('******'+val);
                
                // console.log('<><><><>>??????'+Object.entries(Addressdet));
                // obj)
                // var addrescm=Addressdet[0].address_components;
                //   for(var key in Addressdet) {
                //      var value = objects[key];
                //       console.log('<><><>inside <>>??????'+value);
                //  }
             
                
            }
	
        });
        $A.enqueueAction(action1);
        
    },
    insertRecords:function(component,data){
        
        console.log('?????New Method??????');
          for(var prop in data) {
                  console.log(prop,data[prop]);  
                }
        var d=data;
         var action1 = component.get("c.processWebRes");
        action1.setParams({
            "Res":JSON.stringify(d)
        });

        action1.setCallback(this, function (response) {
            
             var state = response.getState();
            if (state === "SUCCESS") {
                
            }
            
        });
        $A.enqueueAction(action1);
        
                
        
    },

    openListbox: function (component, searchKey) {
        var searchLookup = component.find("searchLookup");

        if (typeof searchKey === 'undefined' || searchKey.length < 3)
        {
            $A.util.addClass(searchLookup, 'slds-combobox-lookup');
            $A.util.removeClass(searchLookup, 'slds-is-open');
            return;
        }

        $A.util.addClass(searchLookup, 'slds-is-open');
        $A.util.removeClass(searchLookup, 'slds-combobox-lookup');
    },

    clearComponentConfig: function (component) {
        var searchLookup = component.find("searchLookup");
        $A.util.addClass(searchLookup, 'slds-combobox-lookup');

        component.set("v.selectedOption", null);
        component.set("v.searchKey", null);

        var iconDirection = component.find("iconDirection");
        $A.util.removeClass(iconDirection, 'slds-input-has-icon_right');
        $A.util.addClass(iconDirection, 'slds-input-has-icon_left');
    },

})