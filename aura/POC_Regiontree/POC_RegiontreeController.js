({
    loadOptions: function (component, event, helper) {
        var opts = [
            { value: "de", label: "German" },
            { value: "es", label: "Spanish" },
            { value: "ta", label: "Tamil" },
            { value: "en", label: "English" }
         ];
         component.set("v.options", opts);
    },
    handleSelect:function(component,event,helper){
       var selected = event.getSource().get("v.value");
        console.log('>>>>>>>>>>><<<<<<<<<<<<<<<'+selected);
        
    },
     keyPressController: function (component, event, helper) {

        var searchKey = component.get("v.searchKey");
         var Language = component.get("v.selectedValue");
        console.log('>>>>>>>>>>><<<Selected Language<<<<<<<<<<<<'+Language);

        helper.openListbox(component, searchKey);
        helper.displayOptionsLocation(component, searchKey,Language);
    },

    selectOption: function (component, event, helper) {
        var selectedItem = event.currentTarget.dataset.record;
        console.log('@@@Selected Item@@'+selectedItem);
        var selectedValue = event.currentTarget.dataset.value;
        console.log('@@@@@Selected Value@@@@@@@@'+selectedValue);
       var selectedPlaceHolder = event.currentTarget.dataset.placeid;
        console.log('@@@@@Selected Place holder@@@@@@@@'+selectedPlaceHolder);
         var Language1 = component.get("v.selectedValue");
        console.log('>>>>>>>>>>><<<Selected Language<<<<<<<<<<<<'+Language1);
        
        
         var locaval2 = event.currentTarget.dataset.locaval;
       console.log('@@@@@Selected Place holder eeee Terms@@@@@@@@'+locaval2);
        
      

        if(Language1 !== 'en'){
            console.log('<<This is not english. Need to make another callout to google api and insert records in db>>')

            helper.displayOptionDetails(component,selectedPlaceHolder);
        }else{
            console.log('<<No call out for english . insert the records in to db>>')
            helper.sendSelectedOption(component,locaval2);
        }

        component.set("v.selectedOption", selectedItem);

        var searchLookup = component.find("searchLookup");
        $A.util.removeClass(searchLookup, 'slds-is-open');

        var iconDirection = component.find("iconDirection");
        $A.util.removeClass(iconDirection, 'slds-input-has-icon_left');
        $A.util.addClass(iconDirection, 'slds-input-has-icon_right');

        component.set("v.searchKey", selectedItem);

    },

    clear: function (component, event, helper) {
        helper.clearComponentConfig(component);
    },

})