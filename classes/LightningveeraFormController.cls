global class LightningveeraFormController {

@AuraEnabled
global static String checkPortal(String username,String password,String startUrl){

system.debug('@@@@@@@@@User Name@@@@@@');
system.debug('@@@@@@@@@Password@@@@@@');

    try{
            ApexPages.PageReference lgn = Site.login(username, password, 'https://veera2-developer-edition.ap5.force.com/picklist_check');
            aura.redirect(lgn);
            return null;
        }
        catch (Exception ex) {
            return ex.getMessage();            
        }

        //return ('Hello from the server--: ' + username+' --Password from server--:'+ password);

 
    // default login process
    //return Site.login(this.username, this.password, this.START_URL);

}


/**
 * Login controller for the community login 
 *
 * @see         Login.page
 *
 * @version     2016-10-05 kevin.kolditz@die-interaktiven.de      first version
 *           
 */  

/*
global without sharing class TXP_LoginCtr {

  global String username {get; set;}
    global String password {get; set;}
    global final String START_URL {get; set;}

    
   // Checks for a forwardURL param. if empty, login page forwards to '/start'. 
   //Otherwise it forwards to a specific url.
   
  global TXP_LoginCtr () {
    // handled 
    if (System.currentPageReference().getParameters().containsKey('forwardURL')) {

      this.START_URL = System.currentPageReference().getParameters().get('forwardURL');
    } else {

      this.START_URL = '/TXP_PortalHome';
    }
  }

 
   //Check if the user has inputted the correct password and username
   
  public PageReference login() {
    // check if required fields are filled
    if (String.isBlank(this.username) || String.isBlank(this.password)) {

      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Please enter your username and password.'));
      return null;
    }
    
    // default login process
    return Site.login(this.username, this.password, this.START_URL);
  }

}

*/


}