public class PlaceDetails{
    public cls_html_attributions[] html_attributions;
    public cls_result result;
    public String status;   //OK
   
    class cls_html_attributions {
     
    }
    class cls_result {
        public cls_address_components[] address_components;
        public String adr_address;  //Colombo 08, <span class="locality">Colombo</span>, <span class="country-name">Sri Lanka</span>
        public String formatted_address;    //Colombo 08, Colombo, Sri Lanka
        public cls_geometry geometry;
        public String icon; //https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png
        public String id;   //e3994e6c9c632786efc2c71d0dbd117b755c870c
        public String name; //Colombo 08
        public cls_photos[] photos;
        public String place_id; //ChIJ25G5kaJZ4joR_GeKEonHf-M
        public String reference;    //CmRbAAAAeeuSgZ_v6t5wt8hZth7DHlQM-gh7othcfqHEEXTcHqgyIdF9yVlqxCG0rExYS8W-tu4IG6u9VK9RcoNIIUPBQQD8ky6DsjjQ1Sc58phrXHK4mvAy6p24Wq_SRlw1VIe6EhAl1zj2GhWVsvUHpo9c8Ta4GhSA0VZbqszi68Yd4dTdiIJN5S2tiA
        public String scope;    //GOOGLE
        public cls_types[] types;
        public String url;  //https://maps.google.com/?q=Colombo+08,+Colombo,+Sri+Lanka&ftid=0x3ae259a291b991db:0xe37fc789128a67fc
        public Integer utc_offset;  //330
        public String vicinity; //Colombo 08
    }
    class cls_address_components {
        public String long_name;    //Colombo 08
        public String short_name;   //Colombo 08
        public cls_types[] types;
    }
    class cls_types {
       
    }
    class cls_geometry {
        public cls_location location;
        public cls_viewport viewport;
    }
    class cls_location {
        public Double lat;  //6.9121796
        public Double lng;  //79.8828828
    }
    class cls_viewport {
        public cls_northeast northeast;
        public cls_southwest southwest;
    }
    class cls_northeast {
        public Double lat;  //6.925612399999999
        public Double lng;  //79.89004229999999
    }
    class cls_southwest {
        public Double lat;  //6.899252
        public Double lng;  //79.8678491
    }
    class cls_photos {
        public Integer height;  //2988
        public cls_html_attributions[] html_attributions;
        public String photo_reference;  //CmRaAAAA-DXmwjcQzOXUkYQTgfJ6pfo3SkdiDwtZnBuOPZTQSIxnIeNBQQ-RA8d9SXS-ACU-SSzWR3NJxaF9FWSewHcUJUTT0IrWoIQGqph-k-4Mr7fAkei_LipTO8nejjYGgMYnEhAXkpatn3QDXTK1lI75sxs6GhT3edP7kt9F80JFAj6l4aGIkf80vw
        public Integer width;   //5312
    }
    public static PlaceDetails parse(String json){
        return (PlaceDetails) System.JSON.deserialize(json, PlaceDetails.class);
    }

}