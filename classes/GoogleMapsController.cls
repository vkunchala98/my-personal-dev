global class GoogleMapsController{

@AuraEnabled
global static string getAddressAutoComplete(String input, String types,String langug) {
    String url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input='
            + EncodingUtil.urlEncode(input, 'UTF-8')
            + '&types=' + types
            + '&language=' + langug
            + '&key=' + GoogleMapsController.getGoogleMapsAPIKey();
            
    // String url ='https://maps.googleapis.com/maps/api/js?key='+GoogleMapsController.getGoogleMapsAPIKey()+'&libraries=places';
    return GoogleMapsController.getHttp(url);
}

@AuraEnabled
global static string getAddressDetails(String PlaceId) {
    String url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='
            +PlaceId+'&language=en&key='+ GoogleMapsController.getGoogleMapsAPIKey();
     
    // String resp=GoogleMapsController.getHttp(url);   
    // PlaceDetails myresp= PlaceDetails.parse(resp);
    // return null;
    return GoogleMapsController.getHttp(url);
}


public static List<AddressJsonInfo> parse(String json) {
        return (List<AddressJsonInfo>) System.JSON.deserialize(json, List<AddressJsonInfo>.class);
    }
 


global static String getGoogleMapsAPIKey(){

return 'AIzaSyBAQjLj6m6OM7SrR9UiumwXytKJWNa7YnY';
//return 'test';
}

global static string getHttp(String url){

try{

Http http = new Http();
HttpRequest request = new HttpRequest();
request.setEndpoint(url);
request.setMethod('GET');
HttpResponse response = http.send(request);
system.debug('#############Web service response ##################'+response.getBody());

return response.getBody();
}catch(Exception e){

 return 'sample string';
}
} 

//This is for first web service
@AuraEnabled
global static string processWebResp(String Res) {

    system.debug('>>>>>>>MY NEW PR323232>>>>>'+Res);
        
       // String a='0,Palnadi,9,Chhattisgarh,23,India';
     /*   Map<Integer, String> TermsMap = new Map<Integer, String>();
        String[] parts = Res.split(',');
        
        for (integer i = 0; i < parts.size() ; i += 2) {
            TermsMap.put(Integer.valueOf(parts[i]), parts[i + 1]);
        }
        
        for (Integer s : TermsMap.keySet()) {
        System.debug('####Key##'+s+'@@@@Val@@@@'+  TermsMap.get(s));
        }
        System.debug('>>>>######>'+TermsMap );
       System.debug('>>>>#Size#####>'+TermsMap.size() );
       
       */
               
      //  String a='0,Palnadi,9,Chhattisgarh,23,India';
         List<String> myreverse =new List<String>();
          String[] parts = Res.split(',');
        System.debug('####String##'+Res);        
        for(Integer i = parts.size() - 1; i >= 0; i-=2){
            myreverse.add(parts[i]);
        }
        
        System.debug('####My REverse List##'+myreverse);
        
        //Calling method
        insertRegioninDB(myreverse);
     
    return null;

}


public Static void insertRegioninDB(List<String> myreverse){


    Region__c  newRgn = new Region__c();
    Region__c  currnewRgn = new Region__c();

       integer l=0;
    //India,Chhattisgarh,Palnadi
    for(String s:myreverse){
      
    //  Region__c jjk=GoogleMapsController.IsValid(s);
      
   
    Boolean isExistDb = IsValid(s);
    system.debug('&IS IN DB &&&&'+isExistDb +'&&&&&L value&&'+l);
      
        if(isExistDb == false && l==0){
           //New Entry
            newRgn =insertRecreg(s);
            //l++;
           // break;
        }
        if(isExistDb == true && l==0){
        //Already exists  Country query the record and get the record id
           
           newRgn=getExistedREgn(s);
           
           system.debug('>>Country exists in db record>>>>>>'+newRgn);
        }
        if(isExistDb == false && l==1){
            
            //New Entry
            currnewRgn =insertRec(s,newRgn);
            //l++;
            newRgn=currnewRgn;
           // break;
        
        }
        if(isExistDb == true && l==1){
         // newRgn=jjk;
         //Already exists state , query the reccord get the parent region and compare
         
        currnewRgn = getExistedREgn(s);
        //Matching REgion and parent region
            if(currnewRgn.Region_Name_EN__c == s &&  currnewRgn.ParentRegion__r.Name == newRgn.Name ){
                    newRgn=currnewRgn;
            }else{
                //No matching parent , insert this state and map parent
                currnewRgn = insertRec(s,newRgn);
                 newRgn=currnewRgn;
            
            }
        
        
        }
        if(isExistDb == false && l==2){
            
            //New Entry
            currnewRgn =insertRec(s,newRgn);
           // l++;
            newRgn=currnewRgn;
           // break;
        
        }
        if(isExistDb == true && l==2){
          //Already exists
          
        
        
        }
        
        l++;
    }

}



public Static Region__c getExistedREgn(String cntry){

try{

String queryName = '%' + cntry+ '%';
Region__c match=[select id,name,Region_Name_EN__c,ParentRegion__c,Region__c.ParentRegion__r.Name from Region__c where Region_Name_EN__c like:queryName limit 1];

return match;
}
catch(Exception e){
return null;
}



}

public Static Boolean IsValid(String country){


try{
String queryName = '%' + country + '%';
Region__c match=[select id,name,Region_Name_EN__c,ParentRegion__c from Region__c where Region_Name_EN__c like:queryName limit 1];

if(match!=null)
return true;
else 
return false;

//return match;

}catch(Exception e){
return false;

}



}

//This is for second web service 
@AuraEnabled
global static string processWebRes(String Res) {

system.debug('>>>>>>>MY NEW PR>>>>>'+Res);

List<AddressJsonInfo>  mylst= GoogleMapsController.parse(Res);
system.debug('$$$$$$$$$$$$$$$$$'+mylst);

List<String> finallst = new List<String>();
String country='';
String State='';
String city='';


for(AddressJsonInfo oneREc: mylst){
for(String a: oneREc.types){
        if(a=='country'){
           country=oneREc.long_name;
        }
        if(a=='administrative_area_level_1'){
            State=oneREc.long_name;
        }
        if(a=='locality'){
            city=oneREc.long_name;
        }

    }
}
if(!String.isEmpty(country))
finallst.add(country);
if(!String.isEmpty(State))
finallst.add(State);
if(!String.isEmpty(city))
finallst.add(city);

system.debug('&&&&&&&&&&&&&&&&&&&My FinalList&&&&&&&&&&'+finallst);

 
        //Calling method
        insertRegioninDB(finallst);

return null;
}
   
//Wrapper class
public class AddressJsonInfo {
        public String long_name;
        public String short_name;
        public List<String> types;
    }

public Static Region__c insertRecreg(String sd){

        Region__c onerg= new Region__c();
        onerg.Name=sd;
        onerg.Region_Name_EN__c=sd;
        //onerg.ParentRegion__c=ry;
        insert onerg;
        
        return onerg;


}
public Static Region__c insertRec(String sd,Region__c ry){

        Region__c onerg= new Region__c();
        onerg.Name=sd;
        onerg.Region_Name_EN__c=sd;
        onerg.ParentRegion__c=ry.id;
        insert onerg;
        
        return onerg;


}

}