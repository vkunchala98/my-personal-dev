// This is to retrieve the account info and show it on page

public class AccInfo{   //Class declaration

  public List<Account>  AccRecords{set;get;}   //Variable declaration
  
  //Default constructor
  public AccInfo(){
  
    AccRecords= new List<Account>();    //Variable Allocation 
  }
   
  //Method 
  public PageReference AccInfo1(){
     
      List<Account>  AccRecords1 = new List<Account>();
      AccRecords1= [select id,name,Phone,type from Account];  //SOQL Query to retrieve Accounts
      
      for(Account oneAcc : AccRecords1){
           if(oneAcc.Name=='Dickenson plc')
              AccRecords.add(oneAcc);
      }
     
      return null;
  
  }

}