public with sharing class CampingListController {

    @AuraEnabled
    public static List<Camping_Item__c> getItems(){
    
            return [select id,name,Packed__c,Price__c,Quantity__c from Camping_Item__c];
    
    }
    
    @auraenabled
    public static Camping_Item__c saveItem (Camping_Item__c CampingItem){
        insert campingItem;
        return campingItem;
    }



}